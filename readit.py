#!/usr/bin/python3
#readit GPL3
#Copyright (C) 2019 David Hamner

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
import os
import subprocess
from pynput.keyboard import Key, Listener

#use google voice
fuck_google = False

#must be 3 keys or more
key_cmds = {}
key_cmds['read_it'] = ['ctrl', 'alt', 'r']
key_cmds['get_synonym'] = ['ctrl', 'alt', 's']
key_cmds['replace_with_guess'] = ['ctrl', 'alt', 'g']

#############################Command  part####################################
def get_highlighted_text():
    out = subprocess.Popen(['xclip', '-o'], stdout=subprocess.PIPE, 
           stderr=subprocess.STDOUT)
    stdout,stderr = out.communicate()
    return (stdout.decode())
                                                                             #

def cut_to_size(what_to_read):
    MAX_SIZE = 200
    return_data = []
    size = len(what_to_read)
    if size > MAX_SIZE:
        #number of spaces
        index_counter = 0
        spaces_at = []
        for char in what_to_read:
            if char == ' ':
                spaces_at.append(index_counter)
            index_counter = index_counter + 1
        print(what_to_read)
        if len(spaces_at) > 0:
            #split on mid space
            mid_space = int(len(spaces_at)/2)
            space_index = spaces_at[mid_space]
        print(f'cut at: {space_index}')
        split_line = [what_to_read[:space_index],what_to_read[space_index:]]
        for cut_part in split_line:
            if len(cut_part) > MAX_SIZE:
                cut_again = cut_to_size(cut_part)
                for part in cut_again:
                    return_data.append(part)
            else:
                return_data.append(cut_part)
        return(return_data)
    return([what_to_read])


def read(what_to_read):
    if fuck_google:
        pass
    
    if not fuck_google:
        lines = cut_to_size(what_to_read)
        print(lines)
        for line in lines:
            #strip things we can't have
            line = line.replace("'", "").replace('"', '')
            cmd = f'/usr/bin/mplayer -ao alsa -really-quiet -noconsolecontrols "http://translate.google.com/translate_tts?ie=UTF-8&client=tw-ob&q={line}&tl=en"'
            os.system(cmd)
                                                                             #
def get_synonym(of_word):
    if ' ' in of_word:
        word = of_word.split(' ')[0]
    else:
        word = of_word
    out = subprocess.Popen(['wn', word, '-n1', '-synsv'], stdout=subprocess.PIPE, 
           stderr=subprocess.STDOUT)
    stdout,stderr = out.communicate()
    raw_out = stdout.decode()
    try:
        alike = raw_out.split('\n')[-2]
        alike = alike.split('>')[-1]
        #don't read too much info
        if "#1" in alike:
            alike = ""
        alike = raw_out.split('\n')[-3] + alike
    except Exception:
        alike = 'Uhm.'
    return(alike)
                                                                             #
def process_cmd(cmd):
    if cmd == "read_it":
        read(get_highlighted_text())
    if cmd == "get_synonym":
        read(get_synonym(get_highlighted_text()))
                                                                             #
                                                                             #
#########################Keyabord reading part################################
keys_down = []
def on_press(key):
    global keys_down
    #clean up key name:
    try:
        key = key.char
    except AttributeError:
        key = key.name
                                                                             #
    #print(f'{keys_down} down')
    if key not in keys_down:
        keys_down.append(key)
                                                                             #
    #only check if this is a key combo
    if len(keys_down) > 2:
        for cmd_name in key_cmds:
            key_to_check = key_cmds[cmd_name]
            run_cmd = True
            for this_key in key_to_check:
                if this_key not in keys_down:
                    run_cmd = False
                    break
            if run_cmd:
                print("run: " + cmd_name)
                process_cmd(cmd_name)
            else:
                print(keys_down)
                                                                             #
def on_release(key):
    global keys_down
                                                                             #
    #clean up key name:
    try:
        key = key.char
    except AttributeError:
        key = key.name
                                                                             #
    #print(f'{key} up')
    if key in keys_down:
        keys_down.remove(key)
    #if key == Key.esc:
        # Stop listener
        #return False
                                                                             #
# Collect events until released
with Listener(on_press=on_press,on_release=on_release) as listener:
    listener.join()
##############################################################################
